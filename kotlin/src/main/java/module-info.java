module example {
    requires tornado.api;
    requires kotlin.stdlib;

    exports org.example;
    opens org.example;
}
