/*
 * Copyright (c) 2013-2023, APT Group, Department of Computer Science,
 * The University of Manchester.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.example

import uk.ac.manchester.tornado.api.TaskGraph
import uk.ac.manchester.tornado.api.TornadoExecutionPlan
import uk.ac.manchester.tornado.api.annotations.Parallel
import uk.ac.manchester.tornado.api.enums.DataTransferMode
import uk.ac.manchester.tornado.api.runtime.TornadoRuntime
import uk.ac.manchester.tornado.api.types.arrays.FloatArray
import java.math.BigDecimal

class Compute {
    companion object {
        private const val CHECK = true

        @JvmStatic
        fun compute(array: FloatArray) {
            for (i: @Parallel Int in 0 until array.size) {
                array[i] = array[i] + 100
            }
        }

        @JvmStatic
        fun main(args: Array<String>) {
            var size = 150000000
            if (args.isNotEmpty()) {
                size = args[0].toInt()
            }

            val bytesToAllocate = BigDecimal.valueOf(((size.toLong() * 4).toFloat() * 1E-6.toFloat()).toDouble())
            println("Running with size: $size")
            println("Input size: $bytesToAllocate (MB)")
            val array = FloatArray(size)

            val device = TornadoRuntime.getTornadoRuntime().getDriver(0).getDevice(0)
            val maxDeviceMemory = device.maxAllocMemory
            val mb = maxDeviceMemory * 1E-6
            println("Maximum alloc device memory: $mb (MB)")

            val taskGraph = TaskGraph("s0") //
                .task("t0", ::compute, array) //
                .transferToHost(DataTransferMode.EVERY_EXECUTION, array)

            val immutableTaskGraph = taskGraph.snapshot()
            val executor = TornadoExecutionPlan(immutableTaskGraph)
            executor.execute()

            if (CHECK) {
                var check = true
                for (i in 0 until array.size) {
                    if (array[i] != 100f) {
                        check = false
                        break
                    }
                }
                if (!check) {
                    println("Result wrong")
                } else {
                    println("Result correct")
                }
            }
        }
    }
}
