import org.jetbrains.kotlin.gradle.dsl.JvmTarget

plugins {
    kotlin("jvm") version "1.9.22"
    application
}

dependencies {
    implementation("tornado:tornado-api:1.0.1")
    testImplementation("org.jetbrains.kotlin:kotlin-test")
}

kotlin {
    compilerOptions {
        jvmTarget = JvmTarget.JVM_21
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_21
    targetCompatibility = JavaVersion.VERSION_21
}

application {
    mainClass = "org.example.Compute"
    mainModule = "example"
}

tasks.test {
    useJUnitPlatform()
}

// To compile the module with the kotlin code
tasks.compileJava {
    destinationDirectory = tasks.compileKotlin.get().destinationDirectory
}


