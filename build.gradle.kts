group = "org.example"
version = "1.0-SNAPSHOT"

subprojects {
    repositories {
        mavenCentral()
        maven("https://raw.githubusercontent.com/beehive-lab/tornado/maven-tornadovm")
    }
}
