# TornadoVM Kotlin Test

To execute the java version:
```bash
./gradlew :java:installDist
tornado --debug --module-path "java/build/install/java/lib/java.jar:java/build/install/java/lib/tornado-api-1.0.1.jar" -m example/org.example.Compute
```

To execute the kotlin version:
```bash
./gradlew :kotlin:installDist
tornado --debug --module-path "kotlin/build/install/kotlin/lib/kotlin.jar:kotlin/build/install/kotlin/lib/kotlin-stdlib-1.9.22.jar:kotlin/build/install/kotlin/lib/tornado-api-1.0.1.jar" -m example/org.example.Compute
```
