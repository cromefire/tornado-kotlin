plugins {
    java
    application
}

dependencies {
    implementation("tornado:tornado-api:1.0.1")
}

tasks.test {
    useJUnitPlatform()
}

java {
    sourceCompatibility = JavaVersion.VERSION_21
    targetCompatibility = JavaVersion.VERSION_21
}

application {
    mainClass = "org.example.Compute"
    mainModule = "example"
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("--enable-preview")
}
